﻿#include <iostream>
#include <string>
// В данной функции мы выводим на экран четные или не четные числа
// в зависимости от выбора пользователя.
void ChetNechet(int n, bool Chet)
{
        std::cout << "Resultat: \n";
        for(int i = Chet; i < n; i += 2)
                std::cout << i << std::endl;
}

int main()
{
    using namespace std;
    int n = 0;
    bool chet = 0;
    bool correct = false;

    cout << "Vvedite 4islo: ";
    cin >> n;
    for (int i = 0; i < n; i += 2)
    {
            cout << i << endl;
    }

    cout << "Kakie chisla hotite vivesti?\n" << "Chetnie: 0\n" << "Nechetnie: 1\n";
    // проверяем корректность ввода пользователя
    while (correct == false) 
    {
        string vvod;
        cin >> vvod;
        if ((vvod == "0") || (vvod == "1"))
        {
            correct = true;
            chet = (vvod == "1");
        }
        else
            cout << "\nNe korekktnii vvod, poprobuite snova: ";
    }
    ChetNechet(n, chet);
    
    return 0;
}